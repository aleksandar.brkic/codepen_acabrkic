class modalController{
    init(){
        const modalBtns = document.querySelectorAll(".showModal");
        for(let i = 0; i < modalBtns.length; i++){
            modalBtns[i].addEventListener('click', this.showModal);
        }

        const closeModalBtns = document.querySelectorAll(".hideModal");
        for(let i = 0; i < closeModalBtns.length; i++){
            closeModalBtns[i].addEventListener('click', this.hideModal);
        }
    }

    showModal(){
        document.querySelector(".overlay").style.display="flex";
    }
    hideModal(){
        document.querySelector(".overlay").style.display="none";
    }
}