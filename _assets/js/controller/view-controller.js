class viewController {
    constructor() {
        this.setting = 0;
    }

    init() {

        this.viewButton = document.querySelector(".change-view");
        this.viewButton.addEventListener("click", () => {
            const settings = ['settings-right-order', 'settings-left-order'];
            const container = document.querySelector('.codepen');

            container.classList.remove('settings-right-order', 'settings-left-order');
            this.setting == settings.length ?
                 this.setting = 0 :
                (container.classList.add(settings[this.setting]), this.setting++);

        });

    }
}