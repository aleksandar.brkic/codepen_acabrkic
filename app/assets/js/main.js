"use strict";

(function (viewController, modalController) {
  if (document.querySelector(".change-view")) {
    viewController = new viewController();
    viewController.init();
  }

  if (document.querySelector(".showModal")) {
    modalController = new modalController();
    modalController.init();
  }
})(viewController, modalController);
//# sourceMappingURL=main.js.map
