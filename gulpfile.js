
let yourHostName = "brkic-codepen.test";

// just change stuff below if you'r familiar with NPM
let kirbyAssets = './app/assets/';
let devSource = './_assets/';
let sourceMap = false;
let compressed = false;
let production = false;

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    gulpif = require('gulp-if'),
    cssmin = require('gulp-cssmin'),
    plumber = require('gulp-plumber'),
    cssnano = require('cssnano'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    postcss = require('gulp-postcss'),
    colors = require('ansi-colors'),
    log = require('fancy-log'),
    env = require('minimist')(process.argv.slice(2)),
    browsersync = require("browser-sync").create()

// console
if (env.production) {
    production = true;
    log(colors.blue(' '));
    log(colors.blue(' '));
    log(colors.blue.bold.underline('##################### PRODUCTION MODE ###################'));
    log(colors.blue('# '));
    log(colors.blue('# ') + ' Production = ' + production);
    log(colors.blue('# '));
    log(colors.blue('# ') + ' Help:');
    log(colors.blue('# ') + ' gulp serve');
    log(colors.blue('# ') + ' gulp build');
    log(colors.blue('# ') + ' gulp build --production');
    log(colors.blue('# '));
    log(colors.blue('# ') + ' Assets:');
    log(colors.blue('# ') + ' all assets compressed, minified and uglifyd');
    log(colors.blue('# '));
    log(colors.blue('# ') + ' Tipp: Disable Chrome browser cache if your using browser sync ');
    log(colors.blue('# '));
    log(colors.blue('#########################################################'));
    log(colors.blue(' '));
    log(colors.blue(' '));
} else {
    log(colors.red(' '));
    log(colors.red(' '));
    log(colors.red.bold.underline('##################### DEVELOPMENT MODE ###################'));
    log(colors.red('# '));
    log(colors.red('# ') + ' Production = ' + production);
    log(colors.red('# '));
    log(colors.red('# ') + ' Help:');
    log(colors.red('# ') + ' gulp serve');
    log(colors.red('# ') + ' gulp build');
    log(colors.red('# ') + ' gulp build --production');
    log(colors.red('# '));
    log(colors.red('# ') + ' Assets:');
    log(colors.red('# ') + ' css, js not minified or uglifyd');
    log(colors.red('# '));
    log(colors.red('# ') + ' Tipp: Disable Chrome browser cache if your using browser sync ');
    log(colors.red('# '));
    log(colors.red('#########################################################'));
    log(colors.red(' '));
    log(colors.red(' '));
}


// build
const build = gulp.series(cleanAssets, BuildCss, CopyFonts, uglifyjs, uglifyjsCustomController, vendorJS, copyimages);
exports.default = build;
exports.build = build;

// serve 
const serve = gulp.series(watchFiles, browserSync)
exports.serve = serve


// watch files
function watchFiles(done) {
    gulp.watch(devSource + 'js/vendor/*.js', {
        usePolling: true
    }, gulp.series(vendorJS, browserSyncReload));
    gulp.watch(devSource + 'js/main.js', {
        usePolling: true
    }, gulp.series(uglifyjs, browserSyncReload));
    gulp.watch(devSource + 'js/controller/**/*.js', {
        usePolling: true
    }, gulp.series(uglifyjsCustomController, browserSyncReload));
    gulp.watch(devSource + 'scss/**/*.scss', {
        usePolling: true
    }, gulp.series(BuildCss, browserSyncReload));
    done();
}


// reload sync
function browserSyncReload(done) {
    browsersync.reload();
    done();
}


// browsersync
function browserSync(done) {
    browsersync.init({
        open: true,
        server: "./app"
    });
    done();
}


// build css
function BuildCss(done) {

    var plugins = [
         cssnano()
    ];

    return gulp
        .src(devSource + 'scss/main.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(postcss(plugins))
        .pipe(gulpif(production, cssmin()))
        .pipe(gulpif(!production, sourcemaps.write('.')))
        .pipe(gulp.dest(kirbyAssets + 'css'))
    done();
}


// copy fonts
function CopyFonts(done) {
    return gulp
        .src(devSource + 'fonts/**/*.{ttf,woff,eot,svg,woff2,css}')
        .pipe(gulpif(production, cssmin()))
        .pipe(gulp.dest(kirbyAssets + 'fonts'))
    done();
}


// build js
function uglifyjs(done) {
    return gulp
        .src([
            devSource + 'js/*.js',
            devSource + 'js/main.js'
        ])
        .pipe(plumber())
        .pipe(babel({
            "presets": ["@babel/env"]
        }))
        .pipe(gulpif(production, uglify({
            compress: compressed
        })))
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(concat('main.js'))
        .pipe(gulpif(!production, sourcemaps.write('.')))
        .pipe(gulp.dest(kirbyAssets + 'js/'))
    done();
}


// Custom Controllers
function uglifyjsCustomController(done) {

    return gulp
        .src([
            devSource + 'js/controller/*'
        ])
        .pipe(plumber())
        .pipe(babel({
            "presets": ["@babel/env"]
        }))
        .pipe(uglify({
            compress: compressed
        }))
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(concat('controllers.js'))
        .pipe(gulpif(!production, sourcemaps.write('.')))
        .pipe(gulp.dest(kirbyAssets + 'js/controller'))
    done();
}


// JS Vendor
function vendorJS(done) {

    return gulp
        .src([
            devSource + 'js/vendor/*.js',
        ])
        .pipe(plumber())
        .pipe(uglify({
            compress: compressed
        }))
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(concat('vendor.js'))
        .pipe(gulpif(!production, sourcemaps.write('.')))
        .pipe(gulp.dest(kirbyAssets + 'js/vendor/'))
    done();
}


// Copy Images
function copyimages(done) {
    return gulp
        .src(devSource + 'images/**/*')
        .pipe(gulp.dest(kirbyAssets + 'images/'))
    done();
}


// Clean assets folder
function cleanAssets(done) {
    return gulp
        .src(kirbyAssets + '*', {
            read: false
        })
        .pipe(clean());
    done();
}